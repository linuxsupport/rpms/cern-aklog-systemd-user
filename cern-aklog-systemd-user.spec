Name:		cern-aklog-systemd-user
Version:	1.8
Release:	1%{?dist}
Summary:	cern-aklog-systemd-user	

Group:		CERN/Utilities
License:	BSD	
URL:		https://gitlab.cern.ch/linuxsupport/rpms/cern-aklog-systemd-user
Source0:  %{name}-%{version}.tgz
BuildArch:	noarch

BuildRequires: systemd-rpm-macros

%description
cern-aklog-systemd-user is a simple RPM which packages a systemd user
unit file for use when running with AFS on systemd v239+ (CentOS8)
This is a workaround to provide AFS PAGs through the per-user systemd
instance.
More details on why this workaround exists can be found here:
https://github.com/systemd/systemd/issues/7261

%prep
%setup -q

%build

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_userunitdir}
install -p -m 0644 src/aklog.service %{buildroot}%{_userunitdir}/aklog.service
install -p -m 0644 src/aklog.timer %{buildroot}%{_userunitdir}/aklog.timer
install -d %{buildroot}%{_userpresetdir}
install -p -m 0644 src/95-aklog.preset %{buildroot}%{_userpresetdir}/95-aklog.preset
install -d %{buildroot}/%{_unitdir}
install -p -m 0644 src/user-aklog@.service %{buildroot}%{_unitdir}/user-aklog@.service
install -d %{buildroot}/%{_unitdir}/user@.service.d
install -p -m 0644 src/user-aklog.conf %{buildroot}%{_unitdir}/user@.service.d/user-aklog.conf
install -d %{buildroot}/%{_unitdir}/user@0.service.d
install -p -m 0644 src/0user-aklog.conf %{buildroot}%{_unitdir}/user@0.service.d/user-aklog.conf


%files
%{_userunitdir}/aklog.service
%{_userunitdir}/aklog.timer
%{_userpresetdir}/95-aklog.preset
%{_unitdir}/user-aklog@.service
%dir %{_unitdir}/user@.service.d
%{_unitdir}/user@.service.d/user-aklog.conf
%{_unitdir}/user@0.service.d/user-aklog.conf
%doc README.md

%post
%systemd_user_post aklog.service aklog.timer
%systemd_post user-aklog@.service

%preun
%systemd_user_preun aklog.service aklog.timer
%systemd_post user-aklog@.service

%postun
%systemd_user_postun_with_restart aklog.service aklog.timer
%systemd_postun_with_restart user-aklog@.service

%changelog
* Tue Nov 19 2024 Steve Traylen <steve.traylen@cern.ch> 1.8-1
- Never run user-aklog@0.service for root logins.

* Mon Aug 26 2024 Steve Traylen <steve.traylen@cern.ch> 1.7-1
- Make the frequent klist even quieter.

* Tue Feb 6 2024 Steve Traylen <steve.traylen@cern.ch> 1.6-2
- Remove erroneous o+x on config file.

* Thu Feb 1 2024 Steve Traylen <steve.traylen@cern.ch> 1.6-1
- Make all executions less chatty.

* Mon Jan 8 2024 Steve Traylen <steve.traylen@cern.ch> 1.5-1
 - Addition of user-aklog@<uid>.service required by user@<uid>.service
 - Permit failure of ExecStart klist in systemd user unit

* Mon Aug 8 2022 Steve Traylen <steve.traylen> - 1.4-1
 - Switch from krenew to kinit -R and aklog
 - Explicitly enable the aklog.service and .timer units
 - Include README.md file in RPM.

* Tue Jul 5 2022 Steve Traylen <steve.traylen> - 1.3-1
 - Use macro %t for $XDG_RUNTIME_DIR in unit

* Thu Jun 30 2022 Steve Traylen <steve.traylen> - 1.2-1
 - Remove hard dependency on aklog being present
 - Create and use a pid file for krenew

* Mon Aug 02 2021 Ben Morrice <ben.morrice@cern.ch> - 1.1-1
 - tweak unit file to avoid "Assignment outside of section"

* Fri Jan 17 2020 Steve Traylen <steve.traylen@cern.ch> - 1.0-2
 - BR systemd for systemd rpm macros

* Fri Jan 10 2020 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
 - initial release
